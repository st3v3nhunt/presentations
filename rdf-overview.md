# [RDF](http://en.wikipedia.org/wiki/Resource_Description_Framework) overview

***

# What is it?

**A way of modeling information**

## Data model

A Triple

`subject -> predicate -> object`

***

![RDF model](https://raw.githubusercontent.com/st3v3nhunt/presentations/master/images/rdf-model.png "RDF model")

***

# [Serialisation formats](http://en.wikipedia.org/wiki/Resource_Description_Framework#Serialization_formats)

****

# [N-Triples (.nt)](http://en.wikipedia.org/wiki/N-Triples)

The original

```xml
<http://people.com/fred-truman> <http://xmlns.com/foaf/spec/#term_mbox> "fred@truman.com" .
<http://people.com/fred-truman> <http://xmlns.com/foaf/spec/#term_mbox_sha1sum> "1f2ad151b4f246ba6b7b507c8390193df232174a" .
```

***

# [Turtle (.ttl)](http://en.wikipedia.org/wiki/Turtle_%28syntax%29)

An extension of N-Triples.

```xml
<http://people.com/fred-truman> <http://xmlns.com/foaf/spec/#term_mbox> "fred@truman.com" ;
	<http://xmlns.com/foaf/spec/#term_mbox_sha1sum> "1f2ad151b4f246ba6b7b507c8390193df232174a" .
```

***

# [JSON-LD](http://json-ld.org/)

Relatively new. Idea being it is better than RDF-JSON
* more powerful
* more expressive
* more on trend...

```json
[
  {
    "@id": "http://people.com/fred-truman",
    "http://xmlns.com/foaf/spec/#term_mbox": [
      {
        "@value": "fred@truman.com"
      }
    ],
    "http://xmlns.com/foaf/spec/#term_mbox_sha1sum": [
      {
        "@value": "1f2ad151b4f246ba6b7b507c8390193df232174a"
      }
    ]
  }
]
```

***

# Other serialisations

* [N-Quads](http://en.wikipedia.org/wiki/N-Quads)
* [RDF-XML](http://en.wikipedia.org/wiki/RDF/XML)
* [RDF-JSON](http://www.w3.org/TR/rdf-json/)
* [RDFa](http://en.wikipedia.org/wiki/RDFa)

[RDF Translator](http://rdf-translator.appspot.com/)

***

# Querying

Query language for RDF is [SPARQL](http://www.w3.org/TR/rdf-sparql-query/)

* `ASK` - boolean result of a question

* `CONSTRUCT` - creates new graphs

* `DESCRIBE` - returns triples about the resource

* `SELECT` - returns data

***

# Query Examples (SELECT)

**Given**
`<http://people.com/fred-truman> <http://xmlns.com/foaf/spec/#term_mbox> "fred@truman.com" .`

**When**
```xml
SELECT ?mbox
WHERE
{
  <http://people.com/fred-truman> <http://xmlns.com/foaf/spec/#term_mbox> ?mbox .
}
```

**Then**

|      mbox       |
|:---------------:|
|"fred@truman.com"|

***

# Query Examples (ASK)

* ASK

* DESCRIBE

* CONSTRUCT

***

# Ontology

* Ontology &asymp; Domain model

* [Linked Open Vocabularies](http://lov.okfn.org/dataset/lov/index.html)

* NHSChoices (Health) ontology being developed in-house

***

# Vocabulary

* Vocabulary provides the predicates used within ontologies

* Well known vocabularies

  * [RDFS](http://www.w3.org/TR/rdf-schema/)

  * Web Ontology Language [OWL](http://www.w3.org/TR/2004/REC-owl-features-20040210/) and [OWL2](http://www.w3.org/TR/owl2-overview/)

***

![Owl Species](https://raw.githubusercontent.com/st3v3nhunt/presentations/master/images/three-owls.png "Three OWLs")

***

# Three flavours of OWL

* OWL Lite - hierarchy and simple constraints

* OWL DL - max expressiveness with computational completeness and decidability

* OWL Full - max expressiveness with no computational guarantees

***

# Vocabularies facilitate reasoning

* Making connections not immediately obvious

* Questions not originally known can be asked

* Further reading: [Introduction to Ontologies and Semantic Web](http://www.obitko.com/tutorials/ontologies-semantic-web/)

***

# What does all of this mean?

* It *could* mean a fundamental shift in the way systems are thought of.

* Remove layers of interpretation within the application.

* Directly expose the data and the model.

***

# Using RDF can:

* Reduce the cost of data integrations

* Increase the speed of iteration

* Enable computers to understand

* Provide shared understanding

* Externally expose data in a strongly typed manner enabling others to find answers to questions we have never even considered

* Link data to others, leverage others' data, reduce cost of ownership

***

# What do we do now?

* Model the domain using a system that may or may not be inter-operable with other systems.
* Build a tightly coupled application on top.
* Need to use the data somewhere else, build another application on top.
* Requirements of the application change...model changes...application changes.
* Data integrations are required...model changes...application changes.

***

# What would we do with RDF?

* Model the domain.
* Expose the data.
* Allow people to figure out what they need, let them build applications.
* More people turn up with different ideas, let them build applications.
* Requirements change, SPARQL queries change.

The model does not change in response to the changing application requirements.

***

# Real world example - symptom checker

***
