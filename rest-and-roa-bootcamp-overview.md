# Overview from [Brian Sletten's REST and Resource-Oriented Architecture Bootcamp](https://skillsmatter.com/courses/467-brian-sletten-rest-and-resource-oriented-architecture-bootcamp)

***

# [REST](http://en.wikipedia.org/wiki/Representational_state_transfer)

What is REpresentational State Transfer (REST)?

Read the Wikipedia page...

***

# [Richardson Maturity Model](http://martinfowler.com/articles/richardsonMaturityModel.html)

Describes 4 steps that are commonly followed on the path to building RESTful systems.

**Level 0** - Use HTTP protocol to interact with remote system. Might just be a bunch of methods available for calling at the end of the wire.

**Level 1** - Resources. The notion of resources split into the useful things that can be interacted with.

**Level 2** - HTTP Verbs. Use standard Verbs to interact with the system e.g. no need to have a resource with a URL of `/<resource>/create` and `/<resource>/delete` instead use an appropriate verb and `/<resource>/`

**Level 3** - Hypermedia Controls. [HATEOAS](http://en.wikipedia.org/wiki/HATEOAS). Providing the client with the ability to discover what is available.

***

![Richardson Maturity Model](http://martinfowler.com/articles/images/richardsonMaturityModel/overview.png "Richardson Maturity Model")
Richardson Maturity Model Graphic courtesy of [Martin Fowler](http://martinfowler.com/articles/richardsonMaturityModel.html)

***

# Key concepts:

Identity and Representation. They are different things.

The identity ~~does not~~ **should** not change.

The representation can change.

***

# Identity

## [URIs (Universal Resource Identifier)](http://en.wikipedia.org/wiki/Uniform_resource_identifier), [URLs (Universal Resource Locator)](http://en.wikipedia.org/wiki/Uniform_resource_locator), [URNs (Universal Resource Name)](http://en.wikipedia.org/wiki/Uniform_resource_name):

**URI** - used to identify the resource. Can be combined with the mechanism to locate the resource over a network which results in a URL.

**URL** - how to locate said resource e.g. [http://www.google.com](http://www.google.com), [file:///C:/code/](file:///C:/code/)

**URN** - the name of the resource i.e. `urn:<NID>:<NSS>` where NID is the Namespace IDentifier and NSS is the Namespace Specific String e.g. `urn:isbn:0453002692`

Using this system it is easy to create unique names that do not clash across the web.

**Some conventions:**

  * The information in a URL **should** not have meaning. The URL is the identity. It should not change. Putting stuff in the URL that has meaning increases the likelihood of it needing to change. This is not good.
  * Plurality in URLs is *probably* not a good thing. But...as noted above - doesn't matter too much. Look at database conventions on plurality - there are no plurals.
  * [Cool URIs dont change](http://www.w3.org/Provider/Style/URI)

***

# Representation

Can be changed by:

**Version** Using `Accept` header e.g. `Accept: application/vnd.nhsc.organisation.v1+json`

**Content type (content negotiation)** Using `Accept` header e.g. `Accept: application/json` or `Accept: application/xml`

**Language** Using `Accept-Language` header e.g. `Accept-Language: en-GB`

***

# [Idempotency](http://en.wikipedia.org/wiki/Idempotence)

A fundamental concept in a distributed, fault tolerant, networked system is the ability to repeat the same operation and get the same result.

## Safe Methods

Those that do not modify resources i.e. `GET` and `HEAD`

## Idempotent Methods

Can be repeated N times with the same result. The resource might be different.

e.g. `GET`, `HEAD`, `PUT`, `DELETE`, `OPTIONS`

***

# [HTTP Verbs](http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html)

`GET` - An idempotent mechanism for retrieving a resource.

`POST` - Used for a number of reasons. When a resource will be created and the identity is unknown e.g. an order on a system where the system creates the order.
        There is no way for the client to know what the order will be called.
        When there is a need to partially update an existing resource. A `POST` to a sub resource with the appropriate data will update the resource.

`PUT` - This should be used when you know what the identity of thing that will be created is. e.g. a blog site. A `PUT` will overwrite the resource if it already existed.

`HEAD` - A `GET` with no body. A light weight check of the resource.

`OPTIONS` - Information about what can be done by the client on the resource.

`PATCH` - A partial update in an idempotent manner. Using the If-Match header. A partial update can be sent with the header.
         If the header matches the patch will be applied. If it doesn't, the patch will not be applied.

`DELETE` - Deletes the resource. Can be repeated N times. The result will be the same. In this case the response might be different e.g. `200 (OK)`, `202 (Accepted)` or `204 (No Content)`.

***

# [HTTP Status Codes](http://en.wikipedia.org/wiki/List_of_HTTP_status_codes):

**2XX codes** - Success.

**3XX codes** - Redirection.

**4XX codes** - Client error.

**5XX codes** - Server error.

***

# Hypermedia

All about linking resources together. How can this be done?

A number of formats:

* [Siren](https://github.com/kevinswiber/siren)

* [HAL+JSON](http://tools.ietf.org/html/draft-kelly-json-hal-06)

* [Collection+JSON](http://amundsen.com/media-types/collection/)

Comparison on the formats from Apigee [API Design: Harnessing Hypermedia Types](https://blog.apigee.com/detail/api_design_harnessing_hypermedia_types)

***

# API Design

***

## API Design Languages

**[RAML](http://raml.org/)**

**[Swagger](http://swagger.io/)**

**[API Blueprint](http://apiblueprint.org/)**

***

# API Design tooling

**[Swagger](http://editor.swagger.wordnik.com/#/)**

**[RAML](https://anypoint.mulesoft.com/apiplatform/)**

**[apiary](http://apiary.io/)**

***

# Next time...RDF
